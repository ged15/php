<?php declare(strict_types=1);

use PHPStan\Command\AnalysisResult;
use PHPStan\Command\ErrorFormatter\ErrorFormatter;
use Symfony\Component\Console\Style\OutputStyle;

final class GitlabErrorFormatter implements ErrorFormatter
{
    public function formatErrors(AnalysisResult $analysisResult, OutputStyle $style): int
    {
        if (!$analysisResult->hasErrors()) {
            $style->writeln('[]');
            return 0;
        }

        $errors = [];

        foreach ($analysisResult->getFileSpecificErrors() as $fileSpecificError) {
            $errors[] = [
                'description' => $fileSpecificError->getMessage(),
                'fingerprint' => md5($fileSpecificError->getMessage()),
                'location' => [
                    'path' => $fileSpecificError->getFile(),
                    'lines' => [
                        'begin' => $fileSpecificError->getLine(),
                    ]
                ]
            ];
        }

        $style->writeln(json_encode($errors));

        return 0;
    }
}
